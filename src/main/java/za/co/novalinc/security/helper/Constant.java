/*
 * Copyright (c) 2015 Novalinc Solutions (Pty) Ltd. All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Find more information on http://www.novalinc.net
 */
package za.co.novalinc.security.helper;

/**
 *
 * @author Hlulani Mhlongo <mhlulani@gmail.com>
 */
public interface Constant {

    String OPTION_DS_JNDI_NAME = "dsJndiName";
    String OPTION_PRINCIPALS_QUERY = "principalsQuery";
    String OPTION_ROLES_QUERY = "rolesQuery";
    String OPTION_PROFILES_QUERY = "profilesQuery";
    String OPTION_UPDATE_LOGIN_QUERY = "updateLoginQuery";
    String OPTION_FAILED_LOGIN_QUERY = "failedLoginQuery";
    String OPTION_HASH_ALGORITHM = "hashAlgorithm";
    String OPTION_HASH_ENCODING = "hashEncoding";

    String PROFILE_KEY = "za.co.novalinc.auth.profile";
    String USERNAME_KEY = "javax.security.auth.login.name";
    String PASSWORD_KEY = "javax.security.auth.login.password";

    String LOGGED_IN = "signedIn";
    String USER_ID = "za.co.novalinc.auth.user.id";

}
