/*
 * Copyright (c) 2015 Novalinc Solutions (Pty) Ltd. All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Find more information on http://www.novalinc.net
 */
package za.co.novalinc.security.helper;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author Hlulani Mhlongo <mhlulani@gmail.com>
 */
public abstract class SecurityUtil {

    private static final Logger LOG = Logger.getLogger(SecurityUtil.class.getName());

    public static Connection getConnection(String jndi) throws SQLException {
        try {
            LOG.log(Level.FINE, "Looking up datasource {0}", jndi);
            Context context = new InitialContext();
            DataSource ds = (DataSource) context.lookup(Objects.toString(jndi));
            return ds.getConnection();
        } catch (NamingException | SQLException ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
            throw new SQLException(String.format("Could not get connection: %s", ex.getMessage()));
        }
    }

    // TODO: This should be exterlized to be use by both the login-module and the App
    public static String hash(char[] password, String algorithm, String encoding) {
        try {
            if (algorithm != null) {
                MessageDigest instance = MessageDigest.getInstance(algorithm);
                byte[] digest = instance.digest(new String(password).getBytes(StandardCharsets.UTF_8));
                if ("base64".equalsIgnoreCase(encoding)) {
                    return DatatypeConverter.printBase64Binary(digest);
                }
                return DatatypeConverter.printHexBinary(digest);
            } else {
                return String.valueOf(password);
            }
        } catch (NoSuchAlgorithmException ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
            return null;
        }
    }
}
