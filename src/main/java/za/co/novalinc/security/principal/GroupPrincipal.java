/*
 * Copyright (c) 2015 Novalinc Solutions (Pty) Ltd. All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Find more information on http://www.novalinc.net
 */
package za.co.novalinc.security.principal;

import java.io.Serializable;
import java.security.Principal;
import java.security.acl.Group;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 *
 * @author Hlulani Mhlongo <mhlulani@gmail.com>
 */
public class GroupPrincipal implements Group, Serializable {

    private final String name;
    private final Set<Principal> members;

    public GroupPrincipal(String name) {
        this.name = name;
        members = new HashSet<>();
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public boolean addMember(Principal user) {
        return members.add(user);
    }

    @Override
    public boolean removeMember(Principal user) {
        return members.remove(user);
    }

    @Override
    public boolean isMember(Principal member) {
        return members.contains(member);
    }

    @Override
    public Enumeration<? extends Principal> members() {
        return Collections.enumeration(members);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 61 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GroupPrincipal other = (GroupPrincipal) obj;
        return Objects.equals(this.name, other.name);
    }

    @Override
    public String toString() {
        return String.format("Group(%s)=%s", name, members);
    }
}
