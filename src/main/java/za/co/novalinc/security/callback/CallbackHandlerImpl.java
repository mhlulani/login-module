/*
 * Copyright (c) 2015 Novalinc Solutions (Pty) Ltd. All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Find more information on http://www.novalinc.net
 */
package za.co.novalinc.security.callback;

import java.io.IOException;
import java.util.logging.Logger;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.UnsupportedCallbackException;

/**
 * @author Hlulani Mhlongo <mhlulani@gmail.com>
 *
 * Still having issues with propagation of Principal when using LoginContext to
 * login
 *
 */
public class CallbackHandlerImpl implements CallbackHandler {

    private static final Logger LOG = Logger.getLogger(CallbackHandlerImpl.class.getName());

    private final String username;
    private final String password;

    public CallbackHandlerImpl(String username, String password) {
        this.username = username;
        this.password = password;
    }

    @Override
    public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {

        if (callbacks == null) {
            LOG.warning("No callback types passed");
            return;
        }

        for (Callback cb : callbacks) {
            if (cb instanceof NameCallback) {
                ((NameCallback) cb).setName(username);
            } else if (cb instanceof PasswordCallback) {
                ((PasswordCallback) cb).setPassword(password.toCharArray());
            } else {
                throw new UnsupportedCallbackException(cb, "Only NameCallback, PasswordCallback are supported callbacks");
            }
        }
    }
}
