/*
 * Copyright (c) 2015 Novalinc Solutions (Pty) Ltd. All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Find more information on http://www.novalinc.net
 */
package za.co.novalinc.security;

import java.io.IOException;
import java.security.Principal;
import java.security.acl.Group;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.security.auth.Subject;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.security.auth.login.LoginException;
import javax.security.auth.spi.LoginModule;
import za.co.novalinc.security.helper.Constant;
import za.co.novalinc.security.helper.SecurityUtil;
import za.co.novalinc.security.principal.GroupPrincipal;
import za.co.novalinc.security.principal.UserPrincipal;

/**
 *
 * @author Hlulani Mhlongo <mhlulani@gmail.com>
 */
public class DefaultLoginModule implements LoginModule, Constant {

    private static final Logger LOG = Logger.getLogger(DefaultLoginModule.class.getName());

    private Subject subject;
    private CallbackHandler handler;
    private Map shared;
    private Map options;
    private Principal principal;
    private Group group;

    @Override
    public void initialize(Subject subject, CallbackHandler handler, Map<String, ?> shared, Map<String, ?> options) {
        this.subject = subject;
        this.handler = handler;
        this.shared = shared;
        this.options = options;

        LOG.log(Level.FINE, "Options: {0}", options);
    }

    @Override
    public boolean login() throws LoginException {

        try (Connection conn = SecurityUtil.getConnection((String) options.get(OPTION_DS_JNDI_NAME))) {

            NameCallback nameCallback = new NameCallback("Username:");
            PasswordCallback passwordCallback = new PasswordCallback("Password:", false);

            handler.handle(new Callback[]{nameCallback, passwordCallback});

            shared.put(USERNAME_KEY, nameCallback.getName());
            shared.put(PASSWORD_KEY, passwordCallback.getPassword());
            shared.put(LOGGED_IN, Boolean.FALSE);

            String passwd;
            Long userId;
            // Authenticate
            try (PreparedStatement stmt = conn.prepareStatement((String) options.get(OPTION_PRINCIPALS_QUERY))) {
                stmt.setString(1, nameCallback.getName());
                ResultSet rs = stmt.executeQuery();
                if (!rs.next()) {
                    LOG.log(Level.FINE, "Username {0} was NOT found", nameCallback.getName());
                    return false;
                }
                passwd = rs.getString(1);
                userId = rs.getLong(2);
                shared.put(USER_ID, userId);
            }

            String hashed = SecurityUtil.hash(passwordCallback.getPassword(),
                    (String) options.get(OPTION_HASH_ALGORITHM),
                    (String) options.get(OPTION_HASH_ENCODING));

            if (Objects.equals(passwd, hashed)) {
                shared.put(LOGGED_IN, Boolean.TRUE);

                principal = new UserPrincipal(String.valueOf(userId));

                // Get Roles //TODO this is duplicatated code with subclass
                try (PreparedStatement stmt = conn.prepareStatement((String) options.get(OPTION_ROLES_QUERY))) {
                    stmt.setLong(1, userId);
                    ResultSet rs = stmt.executeQuery();

                    if (rs.next()) { // one time execution
                        if (rs.getMetaData().getColumnCount() > 1) {
                            group = new GroupPrincipal(rs.getString(2)); // Roles
                        } else {
                            group = new GroupPrincipal("Roles"); // Roles
                        }
                        group.addMember(new UserPrincipal(rs.getString(1)));
                    }

                    while (rs.next()) {
                        group.addMember(new UserPrincipal(rs.getString(1)));
                    }
                }
                return true;
            }

        } catch (IOException | UnsupportedCallbackException | SQLException ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
            throw new LoginException(ex.getMessage());
        }
        return false;
    }

    @Override
    public boolean commit() throws LoginException {

        if (!(Boolean) shared.get(LOGGED_IN)) {
            return false;
        }

        subject.getPrincipals().add(principal);

        if (group != null) {
            subject.getPrincipals().add(group);
        }

        LOG.log(Level.FINE, "Commit");
        return true;
    }

    @Override
    public boolean logout() throws LoginException {
        LOG.log(Level.FINE, "logging out {0}", principal);
        subject.getPrincipals().remove(principal);
        principal = null;
        subject.getPrincipals().remove(group);
        return true;
    }

    @Override
    public boolean abort() throws LoginException {
        LOG.log(Level.FINE, "abort");
        return true;
    }
}
