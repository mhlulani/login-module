/*
 * Copyright (c) 2015 Novalinc Solutions (Pty) Ltd. All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Find more information on http://www.novalinc.net
 */
package za.co.novalinc.security;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;
import javax.sql.DataSource;
import javax.xml.bind.DatatypeConverter;
import org.h2.jdbcx.JdbcDataSource;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import za.co.novalinc.security.callback.CallbackHandlerImpl;

/**
 *
 * @author Hlulani Mhlongo <mhlulani@gmail.com>
 */
public class DefaultLoginModuleTest {

    private static final Logger LOG = Logger.getLogger(DefaultLoginModuleTest.class.getName());
    private static final String DB_USER = "sa";
    private static final String DB_PASS = "sa";
    private static final String DB_URL = "jdbc:h2:mem:test;DB_CLOSE_DELAY=-1";
//    private static final String CONTEXT_FACTORY = "org.apache.openejb.client.LocalInitialContextFactory";
    private static final String CONTEXT_FACTORY = "com.sun.jndi.fscontext.RefFSContextFactory";

    private static final String DATABASE_DOMAIN = "DatabaseSecurityDomain";
    private static final String JNDI_MOCK = "login";

    private static LoginContext loginContext;
    private static Context context;
    static String username = "hlulani";
    static char[] password = "mypass".toCharArray();

    public DefaultLoginModuleTest() {
    }

    @BeforeClass
    public static void setUpClass() throws LoginException, SQLException, NamingException {
        setupEnvironment();
        setupDatabase();
        setupData();
//        context = new LoginContext("MySecurityDomain", new CallbackHandlerImpl(username, password));
    }

    @AfterClass
    public static void tearDown() throws LoginException, SQLException, NamingException {

        context.unbind(JNDI_MOCK);
    }

    private static void setupEnvironment() {
        Path configFile = Paths.get("conf", "jaas.conf");
        System.setProperty("java.security.auth.login.config", configFile.toAbsolutePath().normalize().toString());
        System.setProperty(Context.INITIAL_CONTEXT_FACTORY, CONTEXT_FACTORY);
        System.setProperty(Context.PROVIDER_URL, "file:/tmp");
    }

    private static void setupDatabase() throws NamingException {
        JdbcDataSource ds = new JdbcDataSource();
        ds.setURL(DB_URL);
        ds.setUser(DB_USER);
        ds.setPassword(DB_PASS);

        context = new InitialContext();

        context.bind(JNDI_MOCK, ds);
    }

    private static void setupData() throws NamingException, SQLException {
        try (Connection conn = getConnection()) {
            try (Statement stmt = conn.createStatement()) {
                stmt.addBatch("CREATE TABLE t_user (id INTEGER AUTO_INCREMENT, username VARCHAR(30) NOT NULL UNIQUE, passwd VARCHAR(255) NOT NULL, LAST_LOGIN TIMESTAMP, FAILED_ATTEMPTS INTEGER NOT NULL DEFAULT 0, PRIMARY KEY (id)) ");
                stmt.addBatch("CREATE TABLE t_role (id INTEGER, name VARCHAR(45) NOT NULL UNIQUE, PRIMARY KEY(id))");
                stmt.addBatch("CREATE TABLE t_user_role (user_id INTEGER, role_id INTEGER, PRIMARY KEY (user_id, role_id), FOREIGN KEY (user_id) REFERENCES t_user(id), FOREIGN KEY (role_id) REFERENCES t_role(id))");
                stmt.executeBatch();
            }

            try (Statement stmt = conn.createStatement()) {
                String hashed = encryp(password, "sha-1", "base64");
                stmt.addBatch(String.format("INSERT INTO t_user(username, passwd) VALUES ('%s', '%s')", username, hashed));
                stmt.addBatch("INSERT INTO t_role(id, name) VALUES (1, 'Student')");
                stmt.addBatch("INSERT INTO t_role(id, name) VALUES (2, 'Teacher')");
                stmt.addBatch("INSERT INTO t_role(id, name) VALUES (3, 'Principal')");
                stmt.addBatch("INSERT INTO t_role(id, name) VALUES (4, 'Admin')");
                stmt.executeBatch();
            }
            try (Statement stmt = conn.createStatement()) {
                stmt.addBatch(String.format("INSERT INTO t_user_role(user_id, role_id) VALUES ((SELECT id FROM t_user u WHERE u.username='%s'), 1)", username));
                stmt.addBatch(String.format("INSERT INTO t_user_role(user_id, role_id) VALUES ((SELECT id FROM t_user u WHERE u.username='%s'), 2)", username));
                stmt.addBatch(String.format("INSERT INTO t_user_role(user_id, role_id) VALUES ((SELECT id FROM t_user u WHERE u.username='%s'), 3)", username));
                stmt.executeBatch();
            }
        }
    }

    private static Connection getConnection() throws NamingException, SQLException {
        DataSource ds = (DataSource) context.lookup(JNDI_MOCK);
        return ds.getConnection();
    }

    @Test
    public void login() throws LoginException {

        loginContext = new LoginContext(DATABASE_DOMAIN, new CallbackHandlerImpl(username, String.valueOf(password)));
        LOG.info("Sign-in");
        loginContext.login();
        LOG.info("Sign-out");
        loginContext.logout();
    }

    private static String encryp(char[] password, String algo, String encoding) {
        try {
            MessageDigest instance = MessageDigest.getInstance(algo);
            byte[] digest = instance.digest(String.valueOf(password).getBytes());

            if ("base64".equalsIgnoreCase(encoding)) {
                return DatatypeConverter.printBase64Binary(digest);
            }
            return DatatypeConverter.printHexBinary(digest);
        } catch (NoSuchAlgorithmException ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
            return null;
        }
    }

    @Test
    public void encryped() throws NoSuchAlgorithmException {

        String test = "topsecret";

        System.out.printf("'%s' -> '%s' %n", test, encryp(test.toCharArray(), "sha-256", "base64"));
    }
}
