# login-module

Deploying this on Wildfly/Jboss EAP, you need to create a module:
1. ```$ mvn clean install```
2. Copy the ```target/login-module-0.0.0-SNAPSHOT.jar``` to your login module (See [jboss module](conf/modules))
3. Configure the login module on the application server (See application server [jboss/wildfly config](conf/wilfly-jboss.xml))

Also have a look at my test config [here](conf/jaas.conf)

NOTE: ensure that the module name you define as you login module matches the name you configure on the application server
